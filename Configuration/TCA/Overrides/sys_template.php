<?php
defined('TYPO3_MODE') || die();

call_user_func(function() {
    $extensionKey = 'hive_ovr_hhvideoextender';

    // If automatically include of TypoScript is disabled, then you can include it in the (BE) static-template select-box
    if (!isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey])
        || $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'] === '0') {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extensionKey,
            'Configuration/TypoScript',
            'HIVE>Override Hauer-Heinrich Video Extender'
        );
    }
});
