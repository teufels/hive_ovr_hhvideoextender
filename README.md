![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__ovr__hhvideoextender-blue.svg)
![version](https://img.shields.io/badge/version-1.1.*-yellow.svg?style=flat-square)

Override hauerheinrich/hh-video-extender EXT for Cookie Consent Tool Support for embedded youtube & vimeo content
### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_ovr_hhvideoextender`

### Requirements
`hauerheinrich/hh-video-extender`

### How to use
- Install with composer
- (Optional) Import Static Template (before hive_thm_custom)
  - will be automatically imported - on problems deactivate this in the EXT Settings and manually import
- Set plugin.tx_hhvideoextender.cookieTool = <cookiebot || usercentrics>

### Changelog
- 1.0.0 init
- [...see bitbucket Commits]

### Conflict
- usage of TypoScript default previewImage may not work