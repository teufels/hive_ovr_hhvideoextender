<?php
defined('TYPO3_MODE') || die();

call_user_func(function() {
    $extensionKey = 'hive_ovr_hhvideoextender';

    // automatically add TypoScript, can be disabled in the extension configuration (BE)
    if (isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]) &&
        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$extensionKey]['config']['typoScript'] === '1') {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptConstants('@import "EXT:'.$extensionKey.'/Configuration/TypoScript/constants.typoscript"');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup('@import "EXT:'.$extensionKey.'/Configuration/TypoScript/setup.typoscript"');
    }

    // Overwrite Core Classes
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['HauerHeinrich\\HhVideoExtender\\Rendering\\YouTubeRenderer'] = [
        'className' => 'HIVE\\HiveOvrHhvideoextender\\Rendering\\YouTubeRenderer'
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['HauerHeinrich\\HhVideoExtender\\Rendering\\VimeoRenderer'] = [
        'className' => 'HIVE\\HiveOvrHhvideoextender\\Rendering\\VimeoRenderer'
    ];
});
