<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hive_ovr_hhvideoextender"
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['hive_ovr_hhvideoextender'] = [
    'title' => 'HIVE>Override Hauer-Heinrich Video Extender',
    'description' => 'Override hauerheinrich/hh-video-extender EXT for Cookie Consent Tool Support for embedded youtube & vimeo content',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teudels.com',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.1.2',
    'constraints' => [
        'depends' => [
            'hh-video-extender' => '0.1.64-1.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
